var fs = require('fs');
var needle = require('needle');
const url = "https://www.kommersant.ru/";
var cheerio = require('cheerio');

(async () => {
    var req = await needle('get',url);      
    const content = req.body    ;
    const $ = cheerio.load(content)
    var links = $(".hide2 .article_subheader a");
    var newsItems = links.toArray().map(link => {
        return { text: $(link).text(), url: $(link).attr('href') };
    });
    await fs.writeFileSync('file.txt', JSON.stringify(newsItems))
})();